library(ggplot2)

df <- data.frame(x = 1:10,
                 y = c(1,2,3,4,5,6,7,8,9,10),
                 ymin = y - runif(10),
                 ymax = y + c(0.83, 0.78, 0.23, 0.48, 0.79,
				 0.29, 0.69, 0.81, 0.30, 0.13),
                 xmin = x - c(0.83, 0.78, 0.23, 0.48, 0.79,
				 0.29, 0.69, 0.81, 0.30, 0.13),
                 xmax = x + c(0.83, 0.78, 0.23, 0.48, 0.79,
				 0.29, 0.69, 0.81, 0.30, 0.13))

graph2 <- ggplot(data = df,aes(x = x,y = y)) + 
    geom_point() + 
    geom_errorbar(aes(ymin = ymin,ymax = ymax)) + 
    geom_errorbarh(aes(xmin = xmin,xmax = xmax))

graph2

# saving the plot as png 
ggsave("two-error-graph.png", graph2, path = "/Users/DNS/Desktop/RPlotScripts")
