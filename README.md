## Standard Error (SE)

It is the standard deviation of the vector sampling distribution. Calculated as the SD divided by the square root of the sample size. By construction, SE is smaller than SD. With a very big sample size, SE tends toward 0.

### se = sd(vec) / sqrt(length(vec))

![](https://gitlab.com/Antoniii/plot-van-r/-/raw/main/test-graph.png)

![](https://gitlab.com/Antoniii/plot-van-r/-/raw/main/two-error-graph.png)

## Примеры 

Вектор A:
> A <- c(1,2,5,6.4,6.7,7,7,7,8,9,3,4,1.5,0,10,5.1,2.4,3.4, 4.5, 6.7) 

### Среднее арифметическое значение

Среднее арифметическое значение вычисляется путем сложения значения в множестве и затем делением на общее количество значений:

> my_mean <- mean(A)  
> print(my_mean)

### Медиана

Медиана — среднее значение в отсортированном множестве. Если количество значений четное, то медиана будет средним значением двух значений в середине:

> my_median <- median(A)  
> print(my_median)

### Мода

Она показывает самое часто повторяющееся значение. В R нет стандартной встроенной функции для вычисления моды. Но при этом мы можем создать функцию для такого расчёта, смотрите на пример:

> distinct_A <- unique(A)  
> matches <- match(A, distinct_A)  
> table_A <- tabulate(matches)  
> max_A <- which.max(table_A)  
> mode<-distinct_A[max_A]  
> print(mode)  

Вот, что происходит при выполнении этого кода:

* Вычисление отдельных значений из множества.
* Затем функция определяет частоту каждого элемента и создаёт на основе этих данных таблицу.
* И в конце она находит указатель на элемент, который имеет самую высокую частоту появления и возвращает ее в качестве моды.

### Среднеквадратичное отклонение

Среднеквадратичное отклонение — это отклонение от значений среднего арифметического. 

> sd <- sd(A)  
> print(sd)

### Дисперсия 

Дисперсия — это среднеквадратичное отклонение в квадрате:  

> var <- var(A)  
> print(var)

## Sources

* [R](https://www.r-project.org/)
* [R - язык для статистической обработки данных. Часть 2/3](https://nuancesprog.ru/p/8014/)
* [Plotting means and error bars (ggplot2)](http://www.cookbook-r.com/Graphs/Plotting_means_and_error_bars_(ggplot2)/)
* [to plot two error-bars on each point](https://stackoverflow.com/questions/9231702/ggplot2-adding-two-errorbars-to-each-point-in-scatterplot)
* [Barplot with error bars](https://www.r-graph-gallery.com/4-barplot-with-error-bar)
* [How to save a plot using ggplot2 in R?](https://www.geeksforgeeks.org/how-to-save-a-plot-using-ggplot2-in-r/)
* [Вычисляем возраст Вселенной в R](https://habr.com/ru/post/590277/)
